import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotPwaComponent } from './not-pwa.component';

describe('NotPwaComponent', () => {
  let component: NotPwaComponent;
  let fixture: ComponentFixture<NotPwaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotPwaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotPwaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
