import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PwaComponent } from './pwa/pwa.component';
import { NotPwaComponent } from './not-pwa/not-pwa.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'pwa', component: PwaComponent },
  { path: 'not-pwa', component: NotPwaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
